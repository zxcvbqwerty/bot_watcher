import sys
sys.path.append("..")
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import requests
import os
import json
from  watcher import send_mail


OP_DIR = ""
# IS_TESTING true means we are sending training data to verifier
IS_TESTING = True

# we start with a conviction of 70
ENV_CONVICTION = 70

DEFAULT_VERDICT_SERVER="http://161.35.60.7:5000/verdict"


class Watcher:

    DIRECTORY_TO_WATCH = "/tmp/ds"

    def __init__(self):
        self.DIRECTORY_TO_WATCH = OP_DIR + self.DIRECTORY_TO_WATCH
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print ("Error")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print ("Received created event - {}.".format( event.src_path))
            pipeline_start()

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            print ("Received modified event - {}.".format( event.src_path))
            pipeline_start()

def remove_blank_lines_join(data):
    text = os.linesep.join([s for s in data.splitlines() if s])
    return text

def remove_blank_lines(txt):
    ret=""
    for l in txt.split("\n"):
        if l.strip()!='':
            ret += l + "\n"
    return ret

# this is the pipeline
def pipeline_start():
    print("New File Starting pipeline ....")
    process_data()




def process_data():
    verdict_server = os.getenv("VERDICT_SERVER",DEFAULT_VERDICT_SERVER)
    data_in = ""
    file_name = OP_DIR + Watcher.DIRECTORY_TO_WATCH + '/' + 'converted.txt'
    with open(file_name , 'r') as file:
        data_in = file.read()
    data = remove_blank_lines(data_in)
    print("Data sending {}".format(data))
    # verdict_server = "http://161.35.60.7:5000/verdict"
    response = requests.post(verdict_server , json={
        "data": data,
        "isTesting": False
    })
    print(response.json())
    iconviction = os.getenv("ENV_CONVICTION",ENV_CONVICTION)
    list_data = json.loads(response.text)
    op_text = ""
    email_ids = []
    for dt in list_data:
        print(type(dt))
        strConv = dt['conviction']
        iConv = float(strConv)
        #print("**** output from predict {}".format(dt))
        if dt['thoughts'] == 'True' or  iConv >= iconviction:
            print("**** output from predict {}".format(dt))
            if dt['label'] == 'PERSON':
                print("Adding email")
                email_ids.append(dt['entities'])
            else:
                print("Not adding email")
            op_text += dt['message']


    print("Final text to send == {}".format(op_text))
    print("FInal list of mail ids = {}".format(email_ids))
    if len(op_text) >= 1:
        try:
            send_mail.send_mail_tls(op_text)
        except:
            print("Could not send mail")

def main():
    proj_home = os.getenv("PROJ_HOME")
    print("Project Home = {}".format(proj_home))
    global OP_DIR
    OP_DIR = proj_home + OP_DIR
    watch_home = os.getenv("WATCH_HOME", OP_DIR + Watcher.DIRECTORY_TO_WATCH)
    print("Watch Home = {}".format(watch_home))
    if not os.path.exists(watch_home):
        os.makedirs(watch_home)
    w = Watcher()
    w.run()

if __name__ == '__main__':
    main()
