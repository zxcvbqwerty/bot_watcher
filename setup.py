from setuptools import setup, find_packages

import os

# Allow to run setup.py from another directory.
os.chdir(os.path.dirname(os.path.abspath(__file__)))

version = '0.0.1'

setup(
    name='watcher',
    version=version,

    packages=find_packages(
        exclude=(
            '.*',
            'EGG-INFO',
            '*.egg-info',
            '_trial*',
            '*.tests',
            '*.tests.*',
            'tests.*',
            'tests',
            'examples.*',
            'examples*',
            )
        ),
    include_package_data=True,
    install_requires=[
        'watchdog',
        'requests',
        'google-api-python-client',
        'google-auth-oauthlib',
        ],
    entry_points={
        'console_scripts': [
            'watcher = watcher.main:main',
            ],

        },

)