#!/bin/bash
# Create and activate a virtualenv
virtualenv -p python3 watcher-venv
source watcher-venv/bin/activate
pip3 install -r requirements.txt
#watcher-venv/bin/python3.7 -m spacy download en_core_web_md
export PROJ_HOME=/Users/sandkuma/work_dir/localdisk/bot/bot_watcher/
