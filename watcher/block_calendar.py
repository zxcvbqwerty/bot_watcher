from watcher.cal_setup import get_calendar_service
from datetime import datetime, timedelta



def calendar_event(loc,description,sdate_time,edate_time,frequency,attendees):
  event = {
    'summary': 'Meeting',
    'location': loc,
    'description': description,
    'start': {
      'dateTime': sdate_time,
      'timeZone': 'America/Los_Angeles',
    },
    'end': {
      'dateTime': edate_time,
      'timeZone': 'America/Los_Angeles',
    },
    'recurrence': [
      'RRULE:FREQ=DAILY;COUNT=2'
    ],
    'attendees': [
      {'email': 'lpage@example.com'},
      {'email': 'sbrin@example.com'},
    ],
    'reminders': {
      'useDefault': False,
      'overrides': [
        {'method': 'email', 'minutes': 24 * 60},
        {'method': 'popup', 'minutes': 10},
      ],
    },
  }

  service = get_calendar_service()
  event = service.events().insert(calendarId='primary', body=event).execute()
  print ('Event created: %s' % (event.get('htmlLink')))

if __name__ == '__main__':
  loc = "Delhi"
  description= "meeting on action items"
  d = datetime.now().date()
  tomorrow = datetime(d.year, d.month, d.day, 10) + timedelta(days=1)
  sdate_time = tomorrow.isoformat()
  edate_time = (tomorrow + timedelta(hours=1)).isoformat()
  frequency = 'RRULE:FREQ=DAILY;COUNT=2'
  attendees = [
      {'email': 'lpage@example.com'},
      {'email': 'sbrin@example.com'},
    ]
  calendar_event(loc,description,sdate_time,edate_time,frequency,attendees)