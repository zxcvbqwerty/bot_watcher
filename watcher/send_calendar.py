from watcher.cal_setup import get_calendar_service
from datetime import datetime, timedelta

def send_calendar_event(sdate_time,edate_time):
    loc = "Delhi"
    description = "meeting on action items"
    frequency = 'RRULE:FREQ=DAILY;COUNT=2'
    attendees = [
        {'email': 'lpage@example.com'},
        {'email': 'sbrin@example.com'},
    ]
    event = {
        'summary': 'Meeting',
        'location': loc,
        'description': description,
        'start': {
            'dateTime': sdate_time,
            'timeZone': 'America/Los_Angeles',
        },
        'end': {
            'dateTime': edate_time,
            'timeZone': 'America/Los_Angeles',
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
            {'email': 'lpage@example.com'},
            {'email': 'sbrin@example.com'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
    }
    service = get_calendar_service()
    event = service.events().insert(calendarId='primary', body=event).execute()
    print('Event created: %s' % (event.get('htmlLink')))
