import smtplib, ssl
from email.mime.text import MIMEText

sender_email = "sandeep.coder.k3@gmail.com"
receiver_email = "sandeep.coder.k1@gmail.com"

def send_mail(message):
    print("Sending message to gmail {}".format(message))
    port = 465  # For SSL
    #password = input("Type your password and press enter: ")
    password = "45#xPhNV"

    # Create a secure SSL context
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login("sandeep.coder.k3@gmail.com", password)
        server.sendmail(sender_email, receiver_email, message)

## V Imp we need to enable
## https://stackoverflow.com/questions/10147455/how-to-send-an-email-with-gmail-as-provider-using-python
## https://myaccount.google.com/lesssecureapps
## Less secure app access
##Some apps and devices use less secure sign-in technology, which makes your account vulnerable.
## You can turn off access for these apps, which we recommend, or turn it on if you want to use them despite the risks.
## Google will automatically turn this setting OFF if it’s not being used. Learn more
##Allow less secure apps: ON
## https://myaccount.google.com/lesssecureapps
def send_mail_tls(message):
    print("Sending message to gmail {}".format(message))
    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"
    password = "45#xPhNV"
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        #server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        send_mail_body = getFullEmailMsg(message)
        print('Actual mail {}'.format(send_mail_body.as_string()))
        server.sendmail(sender_email, receiver_email, send_mail_body.as_string())
        server.quit()


    print('Mail Sent')


def getFullEmailMsg(message):
    msg = MIMEText(message)
    msg['Subject'] = 'Action Items'
    msg['To'] = "sandeep.coder.k1@gmail.com"
    msg['From'] = sender_email

    return msg